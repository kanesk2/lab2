import socket
import picar_4wd as fc

HOST = "192.168.1.100" # IP address of your Raspberry PI
PORT = 65432          # Port to listen on (non-privileged ports are > 1023)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    direction = None

    try:
        while 1:
            client, clientInfo = s.accept()
            print("server recv from: ", clientInfo)
            data = client.recv(1024)      # receive 1024 Bytes of message in binary format
            if data != b"":
                print(data)                         
            if data == b"87":
                fc.forward(10)
                direction = "up"
            if data == b"83":
                fc.backward(10)
                direction = "down"                
            if data == b"65":
                fc.turn_left(10)
                direction = "left"                    
            if data == b"68":
                fc.turn_right(10)
                direction = "right"
            if direction != None:  
                output = direction + "," + str(fc.cpu_temperature()) + "," + str(fc.power_read())
                client.sendall(output.encode()) # Echo back to client 
                                  
    except:
        fc.stop()   
        print("Closing socket")
        client.close()
        s.close()    