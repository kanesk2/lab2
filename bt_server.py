import bluetooth
import picar_4wd as fc

hostMACAddress = "DC:A6:32:9F:D8:60" # The address of Raspberry PI Bluetooth adapter on the server. 
port = 0
backlog = 1
size = 1024
s = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
s.bind((hostMACAddress, port))
s.listen(backlog)
print("listening on port ", port)
try:
    client, clientInfo = s.accept()
    while 1:   
        print("server recv from: ", clientInfo)
        data = client.recv(size)
        if data != b"":
            print(data)                     
        if data == b"up":
            fc.forward(10)
        if data == b"down":
            fc.backward(10)           
        if data == b"left":
            fc.turn_left(10)                  
        if data == b"right":
            fc.turn_right(10)
        data = str(fc.cpu_temperature()) + "," + str(fc.power_read())
        data = data.encode()
        client.send(data) # Echo back to client           

except: 
    print("Closing socket")
    client.close()
    s.close()